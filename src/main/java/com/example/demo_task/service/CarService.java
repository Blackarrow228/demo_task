package com.example.demo_task.service;

import com.example.demo_task.model.Car;
import com.example.demo_task.model.Request;

import java.util.List;

public interface CarService {

    void create(Car car);

    List<Car> readAll();

    List<Car> filterRead(Car car);

    Boolean selectionCar(long id, Request request);

    void update(long id, Boolean newState);
}
