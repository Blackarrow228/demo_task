package com.example.demo_task.service;

import com.example.demo_task.model.Car;
import com.example.demo_task.model.Request;
import com.example.demo_task.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService{


    private static final AtomicLong CAR_ID_HOLDER = new AtomicLong();
    private final CarRepository carRepository;

    @Autowired
    public CarServiceImpl (CarRepository carRepository){
        this.carRepository = carRepository;
    }

    @Override
    public void create(Car car) {
        final Long carId = CAR_ID_HOLDER.incrementAndGet();
        car.setId(carId);
        carRepository.save(car);
    }

    @Override
    public List<Car> readAll() {
        return (List<Car>) carRepository.findAll();
    }

    @Override
    public List<Car> filterRead (Car car){
        List<Car> cars = readAll();
        if(car.getLicenseType() != null) {
            cars = cars.stream().filter(a -> a.getLicenseType().equals(car.getLicenseType()))
                    .collect(Collectors.toList());
        }
        if(car.getName() != null) {
            cars = cars.stream().filter(a -> a.getName().equals(car.getName()))
                    .collect(Collectors.toList());
        }
        if(car.getDriveExp() != null) {
            cars = cars.stream().filter(a -> a.getDriveExp().equals(car.getDriveExp()))
                    .collect(Collectors.toList());
        }
        if(car.getBodyCar() != null) {
            cars = cars.stream().filter(a -> a.getBodyCar().equals(car.getBodyCar()))
                    .collect(Collectors.toList());
        }
        return cars;
    }

    @Override
    public Boolean selectionCar(long id, Request request) {

        return Duration.between(request.getInTimeStart(), request.getInTimeEnd()).toHours() < 24 &&
                carRepository.findCarById(id).getDriveExp().equals(request.getDriveExp()) &&
                carRepository.findCarById(id).getLicenseType().equals(request.getLicenseType()) &&
                carRepository.findCarById(id).getState();
    }

    @Override
    public void update(long id, Boolean newState) {
        carRepository.findCarById(id).setState(newState);
    }
}
