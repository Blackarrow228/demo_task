package com.example.demo_task.model;

public class Car {

    private long id;
    private String name;
    private Boolean state;
    private String bodyCar;
    private String licenseType;
    private String driveExp;

    public long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public String getBodyCar() {
        return bodyCar;
    }

    public void setBodyCar(String bodyCar) {
        this.bodyCar = bodyCar;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String getDriveExp() {
        return driveExp;
    }

    public void setDriveExp(String driveExp) {
        this.driveExp = driveExp;
    }
}
