package com.example.demo_task.model;

import java.time.LocalDateTime;

public class Request {
    private LocalDateTime inTimeStart;
    private LocalDateTime inTimeEnd;
    private String driveExp;
    private String licenseType;
    //private Car car;

    public LocalDateTime getInTimeStart() {
        return inTimeStart;
    }

    public void setInTimeStart(LocalDateTime inTimeStart) {
        this.inTimeStart = inTimeStart;
    }

    public LocalDateTime getInTimeEnd() {
        return inTimeEnd;
    }

    public void setInTimeEnd(LocalDateTime inTimeEnd) {
        this.inTimeEnd = inTimeEnd;
    }

    public String getDriveExp() {
        return driveExp;
    }

    public void setDriveExp(String driveExp) {
        this.driveExp = driveExp;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    //  public Car getCar() {
    //    return car;
    //}

    //public void setCar(Car car) {
    //    this.car = car;
   // }
}
