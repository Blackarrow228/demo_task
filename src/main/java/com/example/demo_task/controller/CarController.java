package com.example.demo_task.controller;

import com.example.demo_task.model.Car;
import com.example.demo_task.model.Request;
import com.example.demo_task.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/cars")

public class CarController {

    private final CarService carService;

    @Autowired
    public CarController(CarService carService){
        this.carService = carService;
    }

    @PostMapping("/create")
    @ResponseStatus(value = HttpStatus.CREATED)
    public void create(@RequestBody() Car car){
        carService.create(car);
    }

    @GetMapping("/all")
    public List<Car> readAll() {
        return carService.readAll();
    }

    @GetMapping("/filter")
    public List<Car> filterRead(@RequestBody() Car car) {
        return carService.filterRead(car);
    }

    @GetMapping("/{id}")
    public Boolean selectionCar(@PathVariable("id") long id,
                                @RequestBody() Request request) {
        return carService.selectionCar(id, request);
    }

    @PatchMapping("/{id}/state")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void updateState(@PathVariable("id") long id, @RequestBody() String newState) {
        Boolean state = Boolean.valueOf(newState);
        carService.update(id, state);
    }
}
