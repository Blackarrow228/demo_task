package com.example.demo_task.repository;

import com.example.demo_task.model.Car;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class CarRepository implements CrudRepository<Car, Long> {

    private static final Map<Long, Car> CAR_REPOSITORY_MAP = new HashMap<>();

    @Override
    public <S extends Car> S save(S s) {
        CAR_REPOSITORY_MAP.put(s.getId(), s);
        return null;
    }

    @Override
    public <S extends Car> Iterable<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    public Car findCarById(Long id){
        return CAR_REPOSITORY_MAP.get(id);
    }

    @Override
    public Optional<Car> findById(Long aLong) {
        return Optional.of(CAR_REPOSITORY_MAP.get(aLong));
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }


    @Override
    public Iterable<Car> findAll() {
        return new ArrayList<>(CAR_REPOSITORY_MAP.values());
    }

    @Override
    public Iterable<Car> findAllById(Iterable<Long> iterable) {
        return null;
    }


    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {
        CAR_REPOSITORY_MAP.remove(aLong);
    }

    @Override
    public void delete(Car car) {
        CAR_REPOSITORY_MAP.remove(car.getId());
    }

    @Override
    public void deleteAll(Iterable<? extends Car> iterable) {

    }

    @Override
    public void deleteAll() {

    }
}
